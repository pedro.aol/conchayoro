package br.com.conchayoro.entity;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ProdutoTest {
	
	Produto produto;
	
	@Before
	public void setup() {
		produto = new Produto();
		produto.setId(1L);
		produto.setNome("Produto1");
		produto.setPrecoUnitario(0.0);
		produto.setUnidade("cx");
		String texto = produto.toString();
	}

	@Test
	public void testFaixa1() {
		produto.setPrecoUnitario(50.0);		
		assertEquals("Faixa1", produto.getFaixa());		
	}
	
	@Test
	public void testFaixa2() {
		produto.setPrecoUnitario(101.0);		
		assertEquals("Faixa2", produto.getFaixa());		
	}
	
}
